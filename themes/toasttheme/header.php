<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<header class="c-header" role="banner">
		<div class="c-header__left">
			<a class="c-header__brand" href="/"><span>Woon</span>wijzer</a>
		</div>
		<div class="c-header__right">
			<nav class="c-header__nav">
				<a href="/" class="c-header__nav-item">Home</a>
				<a href="/" class="c-header__nav-item">Hoe werkt het</a>
				<a href="/" class="c-header__nav-item">Locaties</a>
				<a href="/" class="c-header__nav-item">Over ons</a>
				<a href="/" class="c-header__nav-item">Contact</a>
			</nav>
			<a href="tel:+31182551255" class="c-header__button">0182-551255</a>
			<a href="tel:+31182551255" class="c-header__toggle">menu</a>
		</div>
	</header>
